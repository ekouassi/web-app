import AuthService from "../services/auth.service";

const user = JSON.parse(localStorage.getItem("user"));
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: { loggedIn: false }, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }, user) {
      AuthService.login(user).then(
        (user) => {
          commit("loginSuccess", user);
          return new Promise.resolve(user);
        },
        (error) => {
          commit("loginFailure");
          return new Promise.reject(error);
        }
      );
    },
    refreshToken({ commit }, accessToken) {
      commit("refreshToken", accessToken);
    },
    logout({ commit }) {
      AuthService.logout();
      commit("logout");
    },
    register() {},
  },
  mutations: {
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      state.status.user = user;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.status.user = null;
    },
    refreshToken(state, accessToken) {
      state.status.loggedIn = true;
      state.status.user = { ...state.status.user, accessToken: accessToken };
    },
    logout(state) {
      state.status.loggedIn = false;
      state.status.user = null;
    },
    registerSuccess() {},
    registerFailure() {},
  },
};
