import { createApp } from "vue";
import App from "./App.vue";
import router from "./routes";
import store from "./store";
import setupInterceptorsService from "./services/setup-interceptors.service";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets";
import { FontAwesomeIcon } from "./plugins/font-awesome";

const app = createApp(App);
setupInterceptorsService(store);

app
  .use(router)
  .use(store)
  .component("font-awesome-icon", FontAwesomeIcon)
  .mount("#app");
