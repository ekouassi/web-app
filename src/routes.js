import { createRouter, createWebHistory } from "vue-router";
const Security = () => import("@/views/security/Index.vue");
const Login = () => import("@/views/security/Login.vue");
const ForgotPassword = () => import("@/views/security/ForgotPassword.vue");
const ResetPassword = () => import("@/views/security/ResetPassword.vue");

const routes = [
  {
    path: "/",
    name: "Security",
    component: Security,
    children: [
      {
        path: "",
        name: "Login",
        component: Login,
        meta: { pageTitle: "AUTHENTIFIEZ-VOUS" },
      },
      {
        path: "mot-de-passe-oublie",
        name: "ForgotPassword",
        component: ForgotPassword,
        meta: { pageTitle: "MOT DE PASSE OUBLIE" },
      },
      {
        path: "reinitiliser-mot-de-passe",
        name: "ResetPassword",
        component: ResetPassword,
        meta: { pageTitle: "REINITIALISER LE MOT DE PASSE" },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
