import httpService from "./http.service";

class AuthService {
  login(user) {
    httpService.post("/security/tokens", user).then((response) => {
      if (response.data?.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
  }

  logout() {
    localStorage.removeItem("user");
  }
}

export default new AuthService();
