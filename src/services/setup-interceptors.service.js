import axiosInstance from "./http.service";
import TokenService from "./token.service";

const setup = (store) => {
  axiosInstance.interceptors.request.use(
    (config) => {
      const token = TokenService.getLocalAccessToken();

      if (token) {
        config.headers["Authorization"] = "Bearer " + token;
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  axiosInstance.interceptors.response.use(
    (response) => {
      return response;
    },
    async (error) => {
      const originalConfig = error.config;

      if (originalConfig.url !== "/security/tokens" && error.response) {
        // Access Token was expired
        if (error.response?.status === 401 && originalConfig?._retry) {
          originalConfig._retry = true;
          try {
            const data = {
              identity: {
                methods: ["refreshToken"],
                refreshToken: {
                  token: {
                    id: TokenService.getLocalRefreshToken,
                  },
                },
              },
            };
            const response = await axiosInstance.post("/security/tokens", data);
            const { accessToken } = response.data;
            store.dispatch("auth/refreshToken", accessToken);
            TokenService.updateLocalAccessToken(accessToken);
            return axiosInstance(originalConfig);
          } catch (_error) {
            return Promise.reject(_error);
          }
        }
      }
      return Promise.reject(error);
    }
  );
};

export default setup;
